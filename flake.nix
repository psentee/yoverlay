# Copyright © 2023 Psentee <psentee@mailbox.org>
#
# This work is free. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License,
# Version 2, as published by Sam Hocevar. See the COPYING file for
# more details.
{
  description = "Bleeding edge FPGA stack";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    systems.url = "github:nix-systems/default";

    abc = {
      url = "github:YosysHQ/abc"; # pinned in yosys, but the pin is updated
      flake = false;
    };

    amaranth = {
      url = "github:amaranth-lang/amaranth";
      flake = false;
    };

    amaranth-boards = {
      url = "github:amaranth-lang/amaranth-boards";
      flake = false;
    };

    apycula = {
      url = "github:YosysHQ/apicula";
      flake = false;
    };

    icestorm = {
      url = "github:YosysHQ/icestorm";
      flake = false;
    };

    mcy = {
      url = "github:YosysHQ/mcy";
      flake = false;
    };

    nextpnr = {
      url = "github:YosysHQ/nextpnr";
      flake = false;
    };

    nextpnr-tests = {
      url = "github:YosysHQ/nextpnr-tests";
      flake = false;
    };

    trellis = {
      url = "github:YosysHQ/prjtrellis";
      flake = false;
    };

    trellis-db = {
      url = "github:YosysHQ/prjtrellis-db";
      flake = false;
    };

    symbiyosys = {
      url = "github:YosysHQ/sby";
      flake = false;
    };

    yosys = {
      url = "github:YosysHQ/yosys";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    systems,
    ...
  } @ inputs: let
    allSystems = import systems;
    inherit (nixpkgs) lib;

    eachSystemWithPkgs = f:
      builtins.listToAttrs
      (builtins.map (system: let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [self.overlays.default];
          };
        in {
          name = system;
          value = f system pkgs;
        })
        allSystems);
  in {
    overlays.default = import ./overlay.nix inputs;

    packages = eachSystemWithPkgs (
      system: pkgs: {
        inherit (pkgs) abc-verifier icestorm mcy nextpnr symbiyosys trellis yosys;
        inherit (pkgs.yoverlay.python3.pkgs) amaranth amaranth-boards apycula;

        default = pkgs.symlinkJoin {
          name = "yoverlay-everything";
          paths = lib.attrValues (lib.filterAttrs (k: _v: k != "default") self.packages.${system});
        };
      }
    );

    devShells = eachSystemWithPkgs (system: pkgs: {
      default = pkgs.mkShell {
        packages = self.packages.${system}.default.paths;
      };
    });

    apps = eachSystemWithPkgs (system: pkgs: {
      self-update = rec {
        type = "app";
        pkg = pkgs.writeShellApplication {
          name = "update";
          runtimeInputs = [
            pkgs.curl
            pkgs.git
            pkgs.git-lfs
            pkgs.jq
          ];
          text = builtins.readFile ./scripts/update.bash;
        };
        program = pkgs.lib.getExe pkg;
      };
    });
  };
}
