# Copyright © 2023 Psentee <psentee@mailbox.org>
#
# This work is free. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License,
# Version 2, as published by Sam Hocevar. See the COPYING file for
# more details.
{
  self,
  nixpkgs,
  systems,
  abc,
  amaranth,
  amaranth-boards,
  apycula,
  icestorm,
  mcy,
  nextpnr,
  nextpnr-tests,
  trellis,
  trellis-db,
  symbiyosys,
  yosys,
  ...
}: final: prev: let
  trellisBaseVersion = "1.4";

  inherit (nixpkgs) lib;

  maybe = condition: value:
    if condition
    then value
    else
      (
        if builtins.isAttrs value
        then {}
        else if builtins.isList value
        then []
        else if builtins.isString value
        then ""
        else builtins.abort "TODO: zero for ${builtins.typeOf value}"
      );

  mustFindSingle = pred:
    lib.lists.findSingle pred
    (builtins.abort "none found")
    (builtins.abort "many found");

  foldPackageTransform = builtins.foldl' (
    pkg: {
      when ? true,
      xform,
    } @ args:
      if when
      then xform pkg
      else pkg
  );

  bump = {
    pkg,
    src ? null,
    version ? null,
    override ? null,
    overrideAttrs ? null,
  }:
    foldPackageTransform pkg [
      {
        # src/srcs + version
        when = src != null;
        xform = pkg: let
          srcOrSrcs =
            if builtins.isList src
            then {srcs = src;}
            else {inherit src;};
          src0 =
            if builtins.isList src
            then builtins.head src
            else src;
          version' =
            if version == null
            then "unstable-${lib.substring 0 8 src0.lastModifiedDate}-${src0.shortRev}"
            else version;
        in
          pkg.overrideAttrs (prev':
            srcOrSrcs
            // {
              version = version';
              name = "${prev'.pname}-${version'}";
            });
      }

      {
        # overrides from pkgs.yoverlay
        xform = pkg: let
          overrides =
            {
              inherit (final.yoverlay) stdenv;
            }
            // (maybe (!(pkg ? pythonModule)) {
              inherit (final.yoverlay) python3;
              python3Packages = final.yoverlay.python3.pkgs;
            });
        in
          pkg.override (args: lib.attrsets.filterAttrs (k: _: builtins.hasAttr k args) overrides);
      }

      {
        # supplied overrideAttrs
        when = overrideAttrs != null;
        xform = pkg: pkg.overrideAttrs overrideAttrs;
      }

      {
        # supplied override
        when = override != null;
        xform = pkg: pkg.override override;
      }
    ];

  # FIXME: is there a nicer approach?
  # -> git -C ~nixpkgs grep 'Did you specify two'
  reSrc = src: name: final.runCommand name src "cp -a ${src} $out";
in {
  ## available customizations/overrides
  ########################################################################
  yoverlay = lib.customisation.makeOverridable lib.id {
    python3 = final.python311;
    stdenv = final.clangStdenv;
  };

  pythonPackagesExtensions =
    prev.pythonPackagesExtensions
    ++ [
      (pyFinal: pyPrev: {
        ## amaranth from an independently updated fork
        ################################################################
        amaranth = bump {
          pkg = pyPrev.amaranth;
          src = amaranth;
          overrideAttrs = final': prev': {
            patches = [];
            # Original postPatch is unnecessary now
            postPatch = ''
              sed -i~ -e '/^def doc_version():$/a\    return "latest"' setup.py
            '';
          };
        };

        ## amaranth-boards
        ################################################################
        amaranth-boards = bump {
          pkg = pyPrev.amaranth-boards;
          src = amaranth-boards;
        };

        ## ap[iy]cula
        ################################################################
        apycula = bump {
          pkg = pyPrev.apycula;
          src = apycula;
          overrideAttrs = prev': {
            gowinDataTarball = ./files/gowin-data.tgz;
            postPatch = ''
              ${prev'.postPatch or ""}
              tar -xzvf $gowinDataTarball
            '';
            # python doens't understand our unstable version
            SETUPTOOLS_SCM_PRETEND_VERSION = "${pyPrev.apycula.version}+unstable";
          };
        };
      })
    ];

  ## abc
  ######################################################################
  abc-verifier = bump {
    pkg = prev.abc-verifier;
    src = abc;
    overrideAttrs = prev': {
      passthru = (prev'.passthru or {}) // {inherit (abc) rev;};
    };
  };

  ## icestorm
  ######################################################################
  icestorm = bump {
    pkg = prev.icestorm;
    src = icestorm;
  };

  ## mcy
  ######################################################################
  mcy = bump {
    pkg = prev.mcy;
    src = mcy;
  };

  ## nextpnr
  ######################################################################
  nextpnr = bump {
    pkg = prev.nextpnr;
    src = [
      (reSrc nextpnr "nextpnr")
      (reSrc nextpnr-tests "nextpnr-tests")
    ];
  };

  ## symbiyosys
  ######################################################################
  symbiyosys = bump {
    pkg = prev.symbiyosys;
    src = symbiyosys;
    overrideAttrs = final': prev': let
      # find python actually used for build, even if user did an override
      python3 = mustFindSingle (lib.attrsets.hasAttr "pythonVersion") final'.nativeCheckInputs;
    in {
      nativeBuildInputs = (prev'.nativeBuildInputs or []) ++ [python3.pkgs.wrapPython];
      propagatedBuildInputs = (prev'.propagatedBuildInputs or []) ++ [python3.pkgs.click];

      postFixup = ''
        ${prev'.postFixup or ""}
        wrapPythonPrograms
      '';

      # bring back tests, but also test that it loads (that injection of click has worked)
      doCheck = true;
      checkPhase = ''
        chmod +x ./sbysrc/sby.py
        ./sbysrc/sby.py --help
        ${prev'.checkPhase}
      '';
    };
  };

  ## trellis
  ######################################################################
  trellis = bump {
    pkg = prev.trellis;
    src = [
      (reSrc trellis "trellis")
      trellis-db # (reSrc trellis-db "trellis-database")
    ];
    overrideAttrs = final': prev': {
      # copy preConfigure because the path to original source was embedded upstream
      database = builtins.elemAt final'.srcs 1;
      preConfigure = ''
        # detect database mismatch
        diff -q -s ./devices.json $database/devices.json

        rmdir database && ln -sfv $database ./database

        cd libtrellis
      '';

      cmakeFlags = assert lib.hasPrefix "-DCURRENT_GIT_VERSION=" (lib.head prev'.cmakeFlags);
        ["-DCURRENT_GIT_VERSION=${trellisBaseVersion}+g${trellis.shortRev}"] ++ builtins.tail prev'.cmakeFlags;
    };
  };

  ## yosys
  ######################################################################
  yosys = let
    ysMakefile = builtins.readFile (yosys.outPath + "/Makefile");
    ysMakefileLines = lib.strings.splitString "\n" ysMakefile;
    ysYosysVerLine = mustFindSingle (lib.strings.hasPrefix "YOSYS_VER :=") ysMakefileLines;
    ysVersion =
      lib.lists.last
      (builtins.split "[[:space:]]*:=[[:space:]]*" ysYosysVerLine);
  in
    bump {
      pkg = prev.yosys;
      src = yosys;
      version = ysVersion;
    };
}
