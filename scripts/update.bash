#!/usr/bin/env bash
#
# Copyright © 2023 Psentee <psentee@mailbox.org>
#
# This work is free. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License,
# Version 2, as published by Sam Hocevar. See the COPYING file for
# more details.

set -e -u -o pipefail

apycula_before="$(jq -r .nodes.apycula.locked.rev flake.lock)"
nix flake update
apycula_after="$(jq -r .nodes.apycula.locked.rev flake.lock)"

if git diff --quiet; then
  echo 'No updates.'
  exit 0
fi

if [[ "${apycula_before}" != "${apycula_after}" ]]; then
  curl --progress-bar -L -o files/gowin-data.tgz https://github.com/YosysHQ/apicula/releases/download/0.0.0.dev/linux-x64-gowin-data.tgz
fi
